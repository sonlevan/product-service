package com.bank.product.controller;

import com.bank.product.entity.Product;
import com.bank.product.model.ProductLimitUpdateRequest;
import com.bank.product.service.ProductService;
import io.swagger.v3.oas.annotations.Operation;
import jakarta.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@Slf4j
@RestController
@RequestMapping("/api")
public class ProductController {

    @Autowired
    private ProductService productService;

    @GetMapping(value = "/v1/products", produces = "application/json")
    @Operation(summary = "Retrieve All Products")
    public ResponseEntity<?> retrieveProducts() {
        return ResponseEntity.ok(productService.retrieveProducts());
    }

    @PostMapping(value = "/v1/product-limit/update", produces = "application/json")
    @Operation(summary = "Update Product Limit")
    public ResponseEntity<?> updateProductLimit(
            @Valid @RequestBody ProductLimitUpdateRequest productLimitUpdateRequest) {
        Optional<Product> productOptional = productService.updateProductLimit(productLimitUpdateRequest);
        return ResponseEntity.ok(productOptional.get());
    }

    @GetMapping(value = "/v1/hello", produces = "application/json")
    @Operation(summary = "Greeting API")
    public ResponseEntity<?> greeting() {
        return ResponseEntity.ok("hello world");
    }
}
